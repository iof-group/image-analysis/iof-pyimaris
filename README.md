# IOF-PyImaris
---

Collection of Imaris Python(3) Extensions from the Imaging and Optics Facility (IOF), Institute of Science and Technology Austria (ISTA)


## Documentation

* [Filament functions](./docs/README.md)
* [Filament-Surface intersection analysis](./docs/README.md)
* [Label image export](./docs/README.md)
* [Track export (Napari compatible)](./docs/README.md)


## Initial Setup

First, create a Python environment:

```
conda create --name ImarisXT37 python=3.7
```

Then, install IOF-PyImaris by

```
conda activate ImarisXT37
pip install -U git+https://git.ista.ac.at/iof-group/image-analysis/IOF-PyImaris
```

Afterwards, set the Python 3.7 Application and add the IOF-PyImaris XTension Folder to Imaris.


```
Imaris->File->Preferences->CustomTools

 -> Python 3.7 Application: <...>\envs\ImarisXT37\python.exe
 -> XTesnionFolder: <...>\envs\ImarisXT\Lib\site-packages\iof_pyimaris
```

## Update
To update an existing installation
```
conda activate ImarisXT37
pip install -U git+https://git.ista.ac.at/iof-group/image-analysis/IOF-PyImaris
```

## Contributing
To write your own Imaris Xtension, create a new .py file below `iof_pyimaris` containing the following comment block at the begining of the file:

```
#  Copyright ISTA
#
#    <CustomTools>
#      <Menu>
#        <Submenu name="IOF-PyImaris">
#          <Submenu name="<OPTIONAL_SUBMENU>">
#
#             <Item name="<NAME_OF_YOUR_XT>" icon="Python3" tooltip="<TOOLTIP>">
#                 <Command>Python3XT::main(%i)</Command>
#             </Item>
#
#           </Submenu>
#         </Submenu>
#       </Item>
#      </Menu>
#    </CustomTools>
```
Your .py file needs to contain a `main(i)` function, which will be called when a user selects `NAME_OF_YOUR_XT` from the `Image Processing` menu. See `ipython_shell.py` as an simple example.


## Acknowledgement
* SWC import/export code is adapted from [PyImarisSWC](https://imaris.oxinst.com/open/view/pyimarisswc) 
* Helpful discussions for efficient label image export at [image.sc](https://forum.image.sc/t/measure-which-objects-colocalise-in-imarisxt/51699/17)
