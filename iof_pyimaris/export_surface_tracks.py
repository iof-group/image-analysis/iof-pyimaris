#
#
#  Export Movie Label Image and Tracks
#  christophsommer23@gmail.com (2024)
#
#  Copyright ISTA
#
#    <CustomTools>
#      <Menu>
#        <Submenu name="IOF-PyImaris">
#           <Submenu name="Tracks">
#
#             <Item name="Export Tracks (napari-compatible)" icon="Python3" tooltip="Export Tracks (napari-compatible)">
#                 <Command>Python3XT::main(%i)</Command>
#             </Item>
#
#           </Submenu>
#         </Submenu>
#       </Item>
#      </Menu>
#    </CustomTools>


import traceback

try:
    # Standard library imports

    # GUI imports
    import tkinter as tk
    from tkinter import messagebox
    from tkinter import filedialog

    # Import ImarisLib
    import ImarisLib

    # Non standard library imports
    import numpy as np
    import pandas as pd

    from _utils import (
        getSurfaceLabelImageTracks_napari,
        getImaris,
        exceptionPrinter,
    )

except:
    print(traceback.format_exc())
    input()


@exceptionPrinter
def main(aImarisId):
    # Create an ImarisLib object
    Imaris, DataSet, Scene = getImaris(aImarisId)

    sel_surfaces = Imaris.GetFactory().ToSurfaces(Imaris.GetSurpassSelection())
    if sel_surfaces is None:
        messagebox.showinfo(title="Abort", message="No Surface to export selected...")
        return

    surface_name = sel_surfaces.GetName()

    root = tk.Tk()
    root.withdraw()

    tracks_fn = filedialog.asksaveasfilename(
        parent=root,
        title="Save as .csv tracks table",
        initialfile=f"{surface_name}.label.tracks.csv",
        defaultextension=".csv",
        filetypes=[("csv file", ".csv")],
    )

    if len(tracks_fn) > 0:
        tracks = getSurfaceLabelImageTracks_napari(sel_surfaces, DataSet)
        if tracks is not None:
            tracks_tab = pd.DataFrame(
                tracks, columns=["TrackID", "t", "z", "y", "x", "imaris_surface_index"]
            )
            tracks_tab = tracks_tab.sort_values(["TrackID", "t"])
            for c in ["TrackID", "t", "imaris_surface_index"]:
                tracks_tab[c] = tracks_tab[c].astype(int)
            tracks_tab.to_csv(tracks_fn, index=False)
            track_msg = f"Tracks of surface {surface_name} exported to {tracks_fn}."
        else:
            track_msg = f"No tracks found for surface {surface_name}."

        messagebox.showinfo(
            title="Track Export",
            message=track_msg,
        )
