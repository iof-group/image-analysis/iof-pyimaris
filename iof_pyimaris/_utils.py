import traceback

try:
    from IPython import embed

    import tkinter as tk
    from tkinter import messagebox
    from tkinter import simpledialog

    # Non standard library imports
    import ImarisLib

    from skimage.transform import resize
    import numpy as np
    from tqdm.auto import trange, tqdm
except:
    print(traceback.format_exc())
    input()


def exceptionPrinter(do_stuff):
    def tmp(*args, **kwargs):
        try:
            do_stuff(*args, **kwargs)
        except Exception:
            print(traceback.format_exc())
            input("Error: hit Return to close...")

    return tmp


def getImaris(aImarisId):

    # Create an ImarisLib object
    vImarisLib = ImarisLib.ImarisLib()

    # Get an imaris object with id aImarisId
    vImaris = vImarisLib.GetApplication(aImarisId)

    # Check if the object is valid
    if vImaris is None:
        tk.Tk().withdraw()
        messagebox.showwarning("Error", "Could not connect to Imaris!")
        raise RuntimeError("Could not connect to Imaris!")

    # Get the dataset
    vDataSet = vImaris.GetDataSet()
    if vDataSet is None:
        tk.Tk().withdraw()
        messagebox.showwarning("Error", "An image must be loaded to run this XTension!")
        RuntimeError("An image must be loaded to run this XTension!")

    scene = vImaris.GetSurpassScene()

    return vImaris, vDataSet, scene


def get_voxel_size_xyz(vDataSet):
    z = (vDataSet.GetExtendMaxZ() - vDataSet.GetExtendMinZ()) / vDataSet.GetSizeZ()
    y = (vDataSet.GetExtendMaxY() - vDataSet.GetExtendMinY()) / vDataSet.GetSizeY()
    x = (vDataSet.GetExtendMaxX() - vDataSet.GetExtendMinX()) / vDataSet.GetSizeX()
    return x, y, z


def getSurfaceLabelImage_txyz(surface, ds):
    # retuns TXYZ
    nSurfaces = len(surface.GetIds())

    if nSurfaces < 2**8:
        dtype = np.uint8
    elif nSurfaces < 2**16:
        dtype = np.uint16
    else:
        dtype = np.uint32

    label_img = np.zeros(
        (ds.GetSizeT(), ds.GetSizeX(), ds.GetSizeY(), ds.GetSizeZ()), dtype=dtype
    )

    voxel_len_x, voxel_len_y, voxel_len_z = get_voxel_size_xyz(ds)

    for i in trange(nSurfaces, desc="Extracting label img"):
        sl = surface.GetSurfaceDataLayout(i)

        block_start_x = int((sl.mExtendMinX - ds.GetExtendMinX()) / voxel_len_x)
        block_end_x = int((sl.mExtendMaxX - ds.GetExtendMinX()) / voxel_len_x + 1)

        block_start_y = int((sl.mExtendMinY - ds.GetExtendMinY()) / voxel_len_y)
        block_end_y = int((sl.mExtendMaxY - ds.GetExtendMinY()) / voxel_len_y + 1)

        block_start_z = int((sl.mExtendMinZ - ds.GetExtendMinZ()) / voxel_len_z)
        block_end_z = int((sl.mExtendMaxZ - ds.GetExtendMinZ()) / voxel_len_z + 1)

        block_start_x = max(0, block_start_x)
        block_start_y = max(0, block_start_y)
        block_start_z = max(0, block_start_z)

        block_end_x = min(label_img.shape[1] - 1, block_end_x)
        block_end_y = min(label_img.shape[2] - 1, block_end_y)
        block_end_z = min(label_img.shape[3] - 1, block_end_z)

        simgle_mask = surface.GetSingleMask(
            i,
            sl.mExtendMinX,
            sl.mExtendMinY,
            sl.mExtendMinZ,
            sl.mExtendMaxX,
            sl.mExtendMaxY,
            sl.mExtendMaxZ,
            block_end_x - block_start_x,
            block_end_y - block_start_y,
            block_end_z - block_start_z,
        )
        arr_single_mask = np.array(simgle_mask.GetDataShorts(), dtype=bool)[0, 0]

        block = label_img[
            surface.GetTimeIndex(i),
            block_start_x:block_end_x,
            block_start_y:block_end_y,
            block_start_z:block_end_z,
        ]

        # binary indexing here to set label id
        if block.shape != arr_single_mask.shape:
            print(
                f"Warning: shape mismatch block != mask :{block.shape} != {arr_single_mask.shape}. Resizing..."
            )
            arr_single_mask = resize(
                arr_single_mask, output_shape=block.shape, order=0
            ).astype(bool)
        block[arr_single_mask] = i + 1

    return label_img


def getSurfaceLabelImageTracks_napari(surface, ds, correct_weird_track_ids=True):

    track_ids = np.array(surface.GetTrackIds())

    if correct_weird_track_ids:
        track_ids -= 1000000000

    edges = np.array(surface.GetTrackEdges())

    assert len(track_ids) == edges.shape[0], "Edges and TrackIds do not match"

    tracks = []

    last_tid = -1
    for tid, edge_ids in tqdm(
        zip(track_ids, edges), desc="Extracting tracks", total=len(track_ids)
    ):
        if last_tid != tid:
            to_add = edge_ids
        else:
            to_add = edge_ids[1:]

        for eid in to_add:
            x, y, z = surface.GetCenterOfMass(eid)[0]

            z -= ds.GetExtendMinZ()
            y -= ds.GetExtendMinY()
            x -= ds.GetExtendMinX()

            tim = surface.GetTimeIndex(eid)

            tracks.append([tid, tim, z, y, x, eid + 1])

        last_tid = tid

    if len(tracks) > 0:
        return np.stack(tracks)
