#
#
#  Export label image of Imaris Surface
#  christophsommer23@gmail.com (2023)
#
#
#  Copyright ISTA
#
#    <CustomTools>
#      <Menu>
#        <Submenu name="IOF-PyImaris">
#           <Submenu name="Surfaces">
#
#             <Item name="Export Label Image" icon="Python3" tooltip="Export Label Image">
#               <Command>Python3XT::main(%i)</Command>
#             </Item>
#
#           </Submenu>
#        </Submenu>
#      </Menu>
#    </CustomTools>

import traceback

try:
    # Standard library imports

    # GUI imports
    import tkinter as tk
    from tkinter import messagebox
    from tkinter import filedialog

    # Import ImarisLib
    import ImarisLib

    # Non standard library imports
    import tifffile
    import numpy as np
    from tqdm.auto import trange
    from _utils import (
        getSurfaceLabelImage_txyz,
        get_voxel_size_xyz,
        getImaris,
        exceptionPrinter,
    )

except:
    print(traceback.format_exc())
    input()


@exceptionPrinter
def main(aImarisId):
    # Create an ImarisLib object
    Imaris, DataSet, Scene = getImaris(aImarisId)

    sel_surfaces = Imaris.GetFactory().ToSurfaces(Imaris.GetSurpassSelection())
    if sel_surfaces is None:
        messagebox.showinfo(title="Abort", message="No Surface to export selected...")
        return

    surface_name = sel_surfaces.GetName()

    root = tk.Tk()
    root.withdraw()

    label_img_fn = filedialog.asksaveasfilename(
        parent=root,
        title="Save as .tif label image",
        initialfile=f"{surface_name}.labels.tif",
        defaultextension=".tif",
        filetypes=[("tif file", ".tif")],
    )

    if len(label_img_fn) > 0:
        label_img = getSurfaceLabelImage_txyz(sel_surfaces, DataSet)
        label_img = label_img.swapaxes(1, 3)[:, :, None]

        x, y, z = get_voxel_size_xyz(DataSet)

        print(f"Writing label image of surface {surface_name} to {label_img_fn}...")
        tifffile.imsave(
            label_img_fn,
            label_img,
            imagej=True,
            resolution=(1 / x, 1 / y),
            metadata={
                "axes": "TZCYX",
                "finterval": DataSet.GetTimePointsDelta(),
                "spacing": z,
                "unit": DataSet.GetUnit(),
            },
        )
