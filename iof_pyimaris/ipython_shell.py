#
#
#  Interactive Python Shell
#
#  Copyright ISTA
#
#    <CustomTools>
#      <Menu>
#        <Submenu name="IOF-PyImaris">
#          <Item name="Interactive Python Shell" icon="Python3" tooltip="IPython">
#            <Command>Python3XT::myIPython(%i)</Command>
#         </Submenu>
#       </Item>
#      </Menu>
#    </CustomTools>


import traceback


try:
    from IPython import embed

    from _utils import exceptionPrinter
    from _utils import getImaris
except:
    print(traceback.format_exc())
    input()


@exceptionPrinter
def myIPython(aImarisId):
    # Create an ImarisLib object
    Imaris, DataSet, Scene = getImaris(aImarisId)

    embed(
        colors="neutral",
        header="Interactive Python Imaris Shell\n\nUse variables\n  * Imaris\n  * DataSet\n  * Scene\n\n(c) ISTA/IOF-pyImaris ",
    )
