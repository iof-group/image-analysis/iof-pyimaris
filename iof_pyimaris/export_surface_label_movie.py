#
#
#  Export Movie Label Image and Tracks
#  christophsommer23@gmail.com (2024)
#
#
#  Copyright ISTA
#
#    <CustomTools>
#      <Menu>
#        <Submenu name="IOF-PyImaris">
#           <Submenu name="Surfaces">
#
#             <Item name="Export Movie Label Image and Tracks" icon="Python3" tooltip="Export Movie Label Image and Tracks">
#               <Command>Python3XT::main(%i)</Command>
#             </Item>
#
#           </Submenu>
#         </Submenu>
#       </Item>
#      </Menu>
#    </CustomTools>


import traceback

try:
    # Standard library imports

    # GUI imports
    import tkinter as tk
    from tkinter import messagebox
    from tkinter import filedialog

    # Import ImarisLib
    import ImarisLib

    # Non standard library imports
    import tifffile
    import numpy as np
    from tqdm.auto import trange, tqdm
    import pandas as pd

    from _utils import (
        getSurfaceLabelImage_txyz,
        getSurfaceLabelImageTracks_napari,
        get_voxel_size_xyz,
        getImaris,
        exceptionPrinter,
    )

except:
    print(traceback.format_exc())
    input()


@exceptionPrinter
def main(aImarisId):
    # Create an ImarisLib object
    Imaris, DataSet, Scene = getImaris(aImarisId)

    sel_surfaces = Imaris.GetFactory().ToSurfaces(Imaris.GetSurpassSelection())
    if sel_surfaces is None:
        messagebox.showinfo(title="Abort", message="No Surface to export selected...")
        return

    surface_name = sel_surfaces.GetName()

    root = tk.Tk()
    root.withdraw()

    label_img_fn = filedialog.asksaveasfilename(
        parent=root,
        title="Save as .tif label image",
        initialfile=f"{surface_name}.labels.tif",
        defaultextension=".tif",
        filetypes=[("tif file", ".tif")],
    )

    if len(label_img_fn) > 0:
        label_img = getSurfaceLabelImage_txyz(sel_surfaces, DataSet)
        label_img = label_img.swapaxes(1, 3)[:, :, None]

        x, y, z = get_voxel_size_xyz(DataSet)

        print(f"Writing label image of surface {surface_name} to {label_img_fn}...")
        tifffile.imsave(
            label_img_fn,
            label_img,
            imagej=True,
            resolution=(1 / x, 1 / y),
            metadata={
                "axes": "TZCYX",
                "finterval": DataSet.GetTimePointsDelta(),
                "spacing": z,
                "unit": DataSet.GetUnit(),
            },
        )

        tracks = getSurfaceLabelImageTracks_napari(sel_surfaces, DataSet)
        track_msg = "\nNo tracks found"
        if tracks is not None:
            tracks_tab = pd.DataFrame(
                tracks, columns=["TrackID", "t", "z", "y", "x", "imaris_surface_index"]
            )
            tracks_tab = tracks_tab.sort_values(["TrackID", "t"])
            track_fn = label_img_fn[:-4] + ".tracks.csv"
            tracks_tab.to_csv(track_fn, index=False)
            track_msg = f"\nCorresponding tracks exported to {track_fn}."

        messagebox.showinfo(
            title="Label Image Export",
            message=f"Label image of surface {surface_name} exported to {label_img_fn}.{track_msg}",
        )
