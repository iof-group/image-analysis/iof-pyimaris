# IOF-PyImaris Documentation
---

## IOF-PyImaris

#### Interactive Python Shell
> An interactive IPython interpreter for development with dataset and scene preloaded to workspace

## 1. IOF-PyImaris -> Surfaces

#### Export Label Image

> Exports the currently selected Imaris Surfaces as label image in tif. Voxel sizes and frame interval are stored ImageJ compatible. Each label will be set as ```Imaris.SurfaceIndex + 1```.

#### Export Movie Label Image and Tracks

> In addition exports the tracks of the selected surface as Napari compatible .csv file. The file contains the center-of-mass and the corresponding Imaris.SurfaceIndex.

## 2. IOF-PyImaris -> Tracks


#### Export Tracks
> Export tracks of the selected surface as Napari compatible .csv file. The file contains the center-of-mass and the corresponding Imaris.SurfaceIndex.

## 3. IOF-PyImaris -> Filaments

#### Export Filament as SWC with Surface Intersection (micron)
> Imaris Python Extension to export extended SWC files from Imaris filaments. The output SWC file contains an additional column with label IDs linking to Imaris Surfaces. Features of Imaris Surfaces are exported in separat tab-separated files.

#### Export Filament as SWCs (micron)
> Imaris Python Extension to export standard SWC files from Imaris filaments with units in microns

#### Export Filament as SWCs (pixel)
> Imaris Python Extension to export standard SWC files from Imaris filaments. 

#### Import SWCs as Filaments (micron)
> Imaris Python Extension to import standard SWC files from Imaris filaments with units in microns

#### Import SWCs as Filaments (pxiel)
> Imaris Python Extension to import standard SWC files from Imaris filaments. 